package startit.comparator;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;

public class Test {

    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
                new Employee(22, "John", 1001),
                new Employee(19, "Steve", 1003),
                new Employee(23, "Kevin", 1005),
                new Employee(20, "Ron", 1010),
                new Employee(18, "Lucy", 1111));

        System.out.println("Before Sorting the Employee data:");
        for (Employee employee : employees) {
            System.out.println(employee);
        }
        Collections.sort(employees, (e1, e2) -> e1.getAge() - e2.getAge());
        System.out.println("\nAfter Sorting the Employee data by Age:");
        for (Employee employee : employees) {
            System.out.println(employee);
        }
        Collections.sort(employees, (e1, e2) -> e1.getName().compareTo(e2.getName()));
        System.out.println("\nAfter Sorting the Employee data by Name:");
        for (Employee employee : employees) {
            System.out.println(employee);
        }
        Collections.sort(employees, (e1, e2) -> e1.getId() - e2.getId());
        System.out.println("\nAfter Sorting the Employee data by Id:");
        for (Employee employee : employees) {
            System.out.println(employee);
        }

    }
}
